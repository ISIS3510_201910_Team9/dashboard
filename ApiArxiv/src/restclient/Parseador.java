package restclient;

import java.util.ArrayList;


public class Parseador 
{
	public ArrayList<Ejercicio> ejercicios;
	int cantidadPacientes;

	public Parseador(String a)
	{	 ejercicios = new ArrayList<Ejercicio>();
	String[] b = a.split("name");
	int i = 1;
	while (i < b.length) {
		//		ejer.add(new ejercicio());
		agregarEjercicio(b[i]);
	//	System.out.println(b[i]);
		i++;
	}
	int cantidadEjercicios = i-1;
//	System.out.println("Cantidad ejercicios: " + cantidadEjercicios);
	}

	private void agregarEjercicio(String lineaPublicacion)
	{
		// TODO Auto-generated method stub

		String nombreEjercicioAsignado;
		int duracion;
		int cantidadREpeticiones;
		String tipoEjercicio;
		boolean completado;
		int circuitos;
		String nombreEjercicio;
		String creation;
		String update;



		String[] idArregloPublicacion = lineaPublicacion.split(",");
		String nombreAutor = idArregloPublicacion[0].substring(4, idArregloPublicacion[0].length()-2);
//		System.out.println("----------------------------------------");
//		System.out.println(nombreAutor);
//		System.out.println("----------------------------------------");
		nombreEjercicioAsignado=nombreAutor;

		String[] idArregloPublicacion2 = lineaPublicacion.split("duracion");
		String descrip = idArregloPublicacion2[1].substring(31, 33);
//		System.out.println("----------------------------------------");
//		System.out.println(descrip);
//		System.out.println("----------------------------------------");
		duracion = Integer.parseInt(descrip);

		String[] idArregloPublicacion3 = lineaPublicacion.split("cantidadRepeticiones");
		String descrip2 = idArregloPublicacion3[1].substring(31, 32);
//		System.out.println("----------------------------------------");
//		System.out.println(descrip2);
//		System.out.println("----------------------------------------");
		circuitos = Integer.parseInt(descrip2);

		String[] idArregloPublicacion4 = lineaPublicacion.split("repeticiones");
		String descrip3 = idArregloPublicacion4[1].substring(31, 33);
//		System.out.println("----------------------------------------");
//		System.out.println(descrip3);
//		System.out.println("----------------------------------------");
		cantidadREpeticiones= Integer.parseInt(descrip3);


		String[] idArregloPublicacion5 = lineaPublicacion.split("nombre");
		String descrip4 = idArregloPublicacion5[1].substring(30, 31+11);
//		System.out.println("----------------------------------------");
//		System.out.println(descrip4);
//		System.out.println("----------------------------------------");
		tipoEjercicio=descrip4;
		nombreEjercicio=descrip4;


		String[] idArregloPublicacion6 = lineaPublicacion.split("createTime");
		String descrip5 = idArregloPublicacion6[1].substring(4, 31);
		//	System.out.println("----------------------------------------");
		//	System.out.println(descrip5);
		//	System.out.println("----------------------------------------");
		creation= descrip5;


		String[] idArregloPublicacion7 = lineaPublicacion.split("updateTime");
		String descrip6 = idArregloPublicacion7[1].substring(4, 31);
		//System.out.println("----------------------------------------");
		//System.out.println(descrip6);
		//System.out.println("----------------------------------------");
		update=descrip6;

		String[] idArregloPublicacion8 = lineaPublicacion.split("completitud");
		//String descrip7 = idArregloPublicacion8[1].substring(37, 60);
		//System.out.println("----------------------------------------");
		//System.out.println(idArregloPublicacion8[1]);
		String descrip7 = idArregloPublicacion8[1].substring(30, 31);
		//System.out.println(descrip7);
		//System.out.println("----------------------------------------");
		if (descrip7.equals("t")) { completado= true;

		}
		else
			completado=false;


		ejercicios.add(new Ejercicio(nombreEjercicioAsignado,duracion, cantidadREpeticiones, tipoEjercicio,
				completado, circuitos, nombreEjercicio,creation, update));
	}



	//10. Longitud de la sesion de trabajo diaria, cantidad de ejercicios asignados al dia
	public int darCantidadEjercicios()
	{
		return ejercicios.size();

	}

	//6. tipo de ejercicio m�s popular 7. Ejercicio m�s popular
	public String tipoMasPopular()
	{
		//int contadorPopular=0;
		String tipoPopular = "";
		for (Ejercicio ejercicio:ejercicios)
		{
			String tipo = ejercicio.tipoEjercicio;
			tipoPopular=tipo;
			//contadorPopular++;
		}
		return tipoPopular;
	}
	
	
	public String tipoMasEjercicio()
	{
		//int contadorPopular=0;
		String tipoPopular = "";
		for (Ejercicio ejercicio:ejercicios)
		{
			String tipo = ejercicio.tipoEjercicio;
			tipoPopular=tipo;
			//contadorPopular++;
		}
		return tipoPopular;
	}
	
	

	//5. Cantidad de pacientes que han completado los ejercicios en 5 dias
	public int darPacientesEjerciciosCincoDias()
	{
		int lamejorrespuesta=0;
		int lacantidadeejercicios=0;
		for (Ejercicio ejercicio:ejercicios)
		{
			if(ejercicio.completado)
			{
				lacantidadeejercicios++;
			}
		}
		if(lacantidadeejercicios/darCantidadEjercicios()>0.5) lamejorrespuesta=1;
		return lamejorrespuesta;
	}


	//4. Pacientes realizaron eejrcicios completos ultimos 5 d�as
	public int pacientesEjercicios5Dias()
	{
		int lamejorrespuesta=0;
		int lacantidadeejercicios=0;
		for (Ejercicio ejercicio:ejercicios)
		{
			if(ejercicio.completado)
			{
				lacantidadeejercicios++;
			}
		}
		if(lacantidadeejercicios
				==darCantidadEjercicios()) lamejorrespuesta=1;
		return lamejorrespuesta;
	}






	public int darCantidadPacientes()
	{ cantidadPacientes=1;
	return 1;
	}
}
