package restclient;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Consumidor {

	final public static String SEPARATOR = ",";
	private static File file;
	private static FileWriter fr;
	private static BufferedWriter bw;
	private static String lineRead;
	private static Parseador par;

	private static int cantidadEjercicios;

	public static void main(String[] args) {
		
	
		
		try {

			lineRead = "";

			file = new File("./ejercicios.json");
			fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			String strUrl = "";
			lineRead = lineRead.replaceAll(" ", "+");
			System.out.println(lineRead);
			strUrl = "https://firestore.googleapis.com/v1beta1/projects/fisiapp-26399/databases/(default)/documents/ColeccionEjercicios/Usuario1/Ejercicios";
			URL url = new URL(strUrl);//your url i.e fetch data from .
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			String a = new String(" ");
			while ((output = br.readLine()) != null) {
				a = a + output;
				bw.write(output);
		//		System.out.println(output);
			}
			par = new Parseador(a);
//			System.out.println(":::::::::::::::::::::::::");
//			System.out.println(a);
		
//			System.out.println(":::::::::::::::::::::::::");
		
			conn.disconnect();


			TimeUnit.MILLISECONDS.sleep(250);

			br.close();
			bw.close();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean fin=false;
		while(fin==false)
		{
			Scanner sc= new Scanner(System.in);
			int preguntaNego;
			
			System.out.println("Introduzca el numero de pregunta que desea resolver acerca de FisiApp:");
			System.out.println("1. �En promedio, cuantos ejercicios son asignados en una sesi�n diar�a?");
			System.out.println("2. �Cu�l es el tipo de ejercicio m�s popular en la aplicaci�n?");
			System.out.println("3. �Cu�l es el ejercicio m�s popular en la aplicaci�n?");
			System.out.println("4. �Cuantos son los pacientes que han realizado sus ejercicios completamente durante los �ltimos 5 d�as?");
			System.out.println("5. �Cuantos pacientes est�n registrados en la aplicaci�n?");
			System.out.println("6. �Cuantos usuarios de la aplicaci�n realizan siempre m�s de la mitad de los ejercicios asignados?");
			preguntaNego= sc.nextInt();
			
			
			switch(preguntaNego)
			{
			
			case 1:
				System.out.println("Longitud de la sesion de trabajo diaria, cantidad de ejercicios asignados al d�a");
				System.out.println(par.darCantidadEjercicios());
			   break;
			case 2:
				System.out.println("Tipo de ejercicio m�s popular");
				System.out.println(par.tipoMasPopular());
			   break;
			   
			case 3:
				System.out.println("Ejercicio m�s popular");
				System.out.println(par.tipoMasEjercicio());
			   break;
			case 4:
				System.out.println("Cantidad de pacientes en 5 d�as, que han completado los ejercicios.");
				System.out.println(par.pacientesEjercicios5Dias());
			   break;
			case 5:
				System.out.println("Pacientes en la aplicacion");
				System.out.println(par.darCantidadPacientes());
			   break;
			   
			case 6:
				System.out.println("Pacientes que realizan m�s de la mitad de los ejercicios");
			
				System.out.println(par.darPacientesEjerciciosCincoDias());
			   break;
			   
			case 999:
				System.out.println("Fin aplicacion");
			
				fin=true;
			   break;
			}
			if (fin==true)
			{
				break;
			}
		}
	}
	//	private static void agregarPublicaci�n(String lineaPublicacion) throws IOException {
	//		String idFinal;
	//		String fechaPublicacion;
	//		String fechaActualizacion;
	//		int aniopublicacion;
	//		int anioActual;
	//		String title;
	//		String summary;
	//		String autores;
	//		String comentarios = "";
	//		String journal = "";
	//		String doi = "";
	//		String Categorias = "";
	//
	//
	//		String[] idArreglo = lineaPublicacion.split("id");
	//		String idPaper = idArreglo[1];
	//		String[] id2 =idPaper.split(">");
	//		String[] id3 =id2[1].split("</");
	//		//System.out.println("::::::::::::::::::::::::://///////////////");
	//		//System.out.println(id3[0]);
	//		idFinal=id3[0];
	//
//			String[] idArregloPublicacion = lineaPublicacion.split("published");
//			String idPublicacion = idArregloPublicacion[1];
//			String[] idpublushed2 =idPublicacion.split(">");
//			String[] idpublished3 =idpublushed2[1].split("</");
//			String[] idpubli4=idpublished3[0].split("Z");
//			String[] aniopub=idpubli4[0].split("-");
//			aniopublicacion=Integer.parseInt(aniopub[0]);
//			//System.out.println(idpubli4[0]);
//			//System.out.println(aniopublicacion);
//			fechaPublicacion=idpubli4[0];
	//
	//
	//		String[] idArregloActualizacion = lineaPublicacion.split("updated");
	//		String idActual = idArregloActualizacion[1];
	//		String[] idactual2 =idActual.split(">");
	//		String[] idactual3 =idactual2[1].split("</");
	//		String[] i4=idactual3[0].split("Z");
	//		String[] anioActual1=i4[0].split("-");
	//		anioActual=Integer.parseInt(anioActual1[0]);
	//		//System.out.println(i4[0]);
	//		//System.out.println(anioActual);
	//		fechaActualizacion=i4[0];
	//
	//		String[] idTitulo = lineaPublicacion.split("title");
	//		String titulo = idTitulo[1];
	//		String[] ititulo2 =titulo.split(">");
	//		String[] ititulo3 =ititulo2[1].split("</");
	//		//System.out.println("::::::::::::::::::::::::://///////////////");
	//		//System.out.println(ititulo3[0]);
	//		title=ititulo3[0];
	//
	//		String[] idSummary = lineaPublicacion.split("summary");
	//		String summary1 = idSummary[1];
	//		String[] isummary2 =summary1.split(">");
	//		String[] isummary3 =isummary2[1].split("</");
	//		//	System.out.println("::::::::::::::::::::::::://///////////////");
	//		//System.out.println(isummary3[0]);
	//		summary=isummary3[0];
	//
	//
	//		//		String[] idAutores = lineaPublicacion.split("author");
	//		//		int autoreslenght = idAutores.length;
	//		//		System.out.println("Cantidad autores" +((autoreslenght/2)-1));
	//		//		String autores1 = idAutores[1];
	//		//		String[] iautores2 =autores1.split(">");
	//		//		String[] iautores3 =iautores2[1].split("</");
	//		//	//	System.out.println("::::::::::::::::::::::::://///////////////");
	//		//		System.out.println(iautores3[0]);
	//		//		summary=iautores3[0];
	//
	//		if(lineaPublicacion.contains("arxiv:comment"))
	//		{
	//			String[] idComentario = lineaPublicacion.split("arxiv:comment");
	//			String comentario1 = idComentario[1];
	//			String[] iComentario2 =comentario1.split(">");
	//			String[] iComentario3 = iComentario2[1].split("</arxiv:comment>");
	//			String[] iComentario4 = iComentario3[0].split("</");
	//			//	System.out.println("::::::::::::::::::::::::://///////////////");
	//			//System.out.println("Los comentarios son:");
	//			//System.out.println(iComentario4[0]);
	//			comentarios=iComentario4[0];
	//		}
	//
	//
	//		if(lineaPublicacion.contains("doi"))
	//		{
	//			String d = new String ("title=\"doi\"");
	//			String[] idComentario = lineaPublicacion.split(d);
	//			if(idComentario.length > 1) {
	//				String comentario1 = idComentario[1];
	//				String[] iComentario2 = comentario1.split(" ");
	//				String d2 = new String("http");
	//				String[] iComentario3 = iComentario2[1].split(d2);
	//				//	System.out.println("::::::::::::::::::::::::://///////////////");
	//				//System.out.println("El doi es:");
	//				doi = "http" + iComentario3[1].substring(0, iComentario3[1].length() - 1);
	//				//System.out.println(doi);
	//			}
	//		
	//		
	//		}
	//		
	//		
	//		
	//		if(lineaPublicacion.contains(   "<arxiv:journal_ref xmlns:arxiv=\"http://arxiv.org/schemas/atom\">"))
	//		{
	//			String[] idComentario = lineaPublicacion.split("<arxiv:journal_ref xmlns:arxiv=\"http://arxiv.org/schemas/atom\">");
	//		
	//			String comentario1 = idComentario[1];
	//			String[] iComentario2 =comentario1.split("</arxiv:journal_ref>");
	//			
	//			//	System.out.println("::::::::::::::::::::::::://///////////////");
	//			//System.out.println("El journal es:");
	//			journal =iComentario2[0];
	//			//System.out.println(journal);
	//		
	//		}
	//		
	//		
	//		if(lineaPublicacion.contains("category term"))
	//		{
	//			Categorias="";
	//			
	//			String[] idComentario = lineaPublicacion.split("category term");
	//			
	//			for(int i=1; i<idComentario.length;i++) 
	//			{
	//				
	//				
	//			
	//				String comentario1 = idComentario[i];
	//				String[] iComentario2 =comentario1.split(" ");
	//			
	//				String iComentario3=iComentario2[0].substring(2, iComentario2[0].length()-1);
	//				//	System.out.println("::::::::::::::::::::::::://///////////////");
	//				Categorias +=  iComentario3 + ";";
	//			}
	//			//System.out.println("Las categorias son:");
	//			//System.out.println(Categorias);
	//			
	//		}
	//		autores="";
	//		
	//		//System.out.println(":::::::::::::::::::::::::");
	//		
	//		String[] b = lineaPublicacion.split("<author>");
	//		//System.out.println(":::::::::::::::::::::::::");
	//		int i=1;
	//		while(i<b.length) {
	//			String[] autorObjeto = b[i].split("</author>");
	//			String afiliacion="";
	//			
	//			String autor = autorObjeto[0];
	//			String[] name  = autor.split("name");
	//			if (autor.contains("<arxiv:affiliation xmlns:arxiv=\"http://arxiv.org/schemas/atom\">")) {
	//			String[] afiliacionObjeto = autor.split( "<arxiv:affiliation xmlns:arxiv=\"http://arxiv.org/schemas/atom\">");
	//			afiliacion = afiliacionObjeto[1].substring(0, afiliacionObjeto[1].length()-24);
	//			//System.out.println(afiliacion);
	//			}
	//			String nombreAutor = name[1].substring(1, name[1].length()-2);
	//			autores+=nombreAutor+";";
	//			i++;
	//			if(autor.contains("<arxiv:affiliation xmlns:arxiv=\"http://arxiv.org/schemas/atom\">")) {
	//		//	String afiliacionAutoryAutor =  nombreAutor+";"+ afiliacion;
	//			//System.out.println(" La afiliaci�n del autor es a:");
	//			//System.out.println(afiliacionAutoryAutor);
	//			}
	//		}
	//		
	//		//System.out.println(autores);
	//		idFinal = idFinal.replaceAll(",", ";");
	//		fechaPublicacion = fechaPublicacion.replaceAll(",", ";");
	//		fechaActualizacion = fechaActualizacion.replaceAll(",", ";");
	//		title = title.replaceAll(",", ";");
	//		summary = summary.replaceAll(",", ";");
	//		autores = autores.replaceAll(",", ";");
	//		comentarios = comentarios.replaceAll(",", ";");
	//		journal = journal.replaceAll(",", ";");
	//		doi = doi.replaceAll(",", ";");
	//		Categorias = Categorias.replaceAll(",", ";");
	//
	//		String line =idFinal + SEPARATOR + fechaPublicacion + SEPARATOR + fechaActualizacion + SEPARATOR +aniopublicacion + SEPARATOR +anioActual + SEPARATOR +title + SEPARATOR +summary + SEPARATOR +autores + SEPARATOR +comentarios + SEPARATOR +journal + SEPARATOR +doi + SEPARATOR +Categorias + "\n";
	//		bw.write(line);
	//	}

	private static void agregarEjercicio(String lineaPublicacion) 
	{
		String nombreEjercicioAsignado;
		int duracion;
		int cantidadREpeticiones;
		String tipoEjercicio;
		boolean completado;
		String video;
		int circuitos;
		String nombreEjercicio;
		Date creation;
		Date update;
		
		String[] idArregloPublicacion = lineaPublicacion.split(",");
		String nombreAutor = idArregloPublicacion[0].substring(4, idArregloPublicacion[0].length()-2);
		System.out.println("----------------------------------------");
		System.out.println(nombreAutor);
		System.out.println("----------------------------------------");
		nombreEjercicioAsignado=nombreAutor;
		
		String[] idArregloPublicacion2 = lineaPublicacion.split("duracion");
		String descrip = idArregloPublicacion2[1].substring(31, 33);
		System.out.println("----------------------------------------");
		System.out.println(descrip);
		System.out.println("----------------------------------------");
		duracion = Integer.parseInt(descrip);
		
		String[] idArregloPublicacion3 = lineaPublicacion.split("cantidadRepeticiones");
		String descrip2 = idArregloPublicacion3[1].substring(31, 32);
		System.out.println("----------------------------------------");
		System.out.println(descrip2);
		System.out.println("----------------------------------------");
		circuitos = Integer.parseInt(descrip2);
		
		String[] idArregloPublicacion4 = lineaPublicacion.split("repeticiones");
		String descrip3 = idArregloPublicacion4[1].substring(31, 33);
		System.out.println("----------------------------------------");
		System.out.println(descrip3);
		System.out.println("----------------------------------------");
		cantidadREpeticiones= Integer.parseInt(descrip3);
		
		
		String[] idArregloPublicacion5 = lineaPublicacion.split("nombre");
		String descrip4 = idArregloPublicacion5[1].substring(30, 31+11);
		System.out.println("----------------------------------------");
		System.out.println(descrip4);
		System.out.println("----------------------------------------");
		tipoEjercicio=descrip4;
		nombreEjercicio=descrip4;
		
		
		String[] idArregloPublicacion6 = lineaPublicacion.split("createTime");
		String descrip5 = idArregloPublicacion6[1].substring(4, 31);
		System.out.println("----------------------------------------");
		System.out.println(descrip5);
		System.out.println("----------------------------------------");
		
		
		String[] idArregloPublicacion7 = lineaPublicacion.split("updateTime");
		String descrip6 = idArregloPublicacion7[1].substring(4, 31);
		System.out.println("----------------------------------------");
		System.out.println(descrip6);
		System.out.println("----------------------------------------");
	
		String[] idArregloPublicacion8 = lineaPublicacion.split("completitud");
		//String descrip7 = idArregloPublicacion8[1].substring(37, 60);
		System.out.println("----------------------------------------");
		//System.out.println(idArregloPublicacion8[1]);
		String descrip7 = idArregloPublicacion8[1].substring(30, 31);
		System.out.println(descrip7);
		System.out.println("----------------------------------------");
		if (descrip7.equals("t")) { completado= true;
		
		}
		else
			completado=false;

	}
	
//	class ejercicio
//	{
//		String nombreEjercicioAsignado;
//		int duracion;
//		int cantidadREpeticiones;
//		String tipoEjercicio;
//		boolean completado;
//		String video;
//		int circuitos;
//		String nombreEjercicio;
//		Date creation;
//		Date update;
//		
//		public String getNombreEjercicioAsignado() {
//			return nombreEjercicioAsignado;
//		}
//		public void setNombreEjercicioAsignado(String nombreEjercicioAsignado) {
//			this.nombreEjercicioAsignado = nombreEjercicioAsignado;
//		}
//		public int getDuracion() {
//			return duracion;
//		}
//		public void setDuracion(int duracion) {
//			this.duracion = duracion;
//		}
//		public int getCantidadREpeticiones() {
//			return cantidadREpeticiones;
//		}
//		public void setCantidadREpeticiones(int cantidadREpeticiones) {
//			this.cantidadREpeticiones = cantidadREpeticiones;
//		}
//		public String getTipoEjercicio() {
//			return tipoEjercicio;
//		}
//		public void setTipoEjercicio(String tipoEjercicio) {
//			this.tipoEjercicio = tipoEjercicio;
//		}
//		public boolean isCompletado() {
//			return completado;
//		}
//		public void setCompletado(boolean completado) {
//			this.completado = completado;
//		}
//		public String getVideo() {
//			return video;
//		}
//		public void setVideo(String video) {
//			this.video = video;
//		}
//		public int getCircuitos() {
//			return circuitos;
//		}
//		public void setCircuitos(int circuitos) {
//			this.circuitos = circuitos;
//		}
//		public String getNombreEjercicio() {
//			return nombreEjercicio;
//		}
//		public void setNombreEjercicio(String nombreEjercicio) {
//			this.nombreEjercicio = nombreEjercicio;
//		}
//		public Date getCreation() {
//			return creation;
//		}
//		public void setCreation(Date creation) {
//			this.creation = creation;
//		}
//		public Date getUpdate() {
//			return update;
//		}
//		public void setUpdate(Date update) {
//			this.update = update;
//		}
//		
//		public void Ejercicio(String nombreEjercicioAsignado, int duracion, int cantidadREpeticiones, String tipoEjercicio,
//				boolean completado, String video, int circuitos, String nombreEjercicio, Date creation, Date update) {
//			
//			this.nombreEjercicioAsignado = nombreEjercicioAsignado;
//			this.duracion = duracion;
//			this.cantidadREpeticiones = cantidadREpeticiones;
//			this.tipoEjercicio = tipoEjercicio;
//			this.completado = completado;
//			this.video = video;
//			this.circuitos = circuitos;
//			this.nombreEjercicio = nombreEjercicio;
//			this.creation = creation;
//			this.update = update;
//		}
//	}

}